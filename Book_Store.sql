create database Storee;
use Storee;


CREATE TABLE  `book`(

   `book_id` int NOT NULL AUTO_INCREMENT ,
  `book_name` varchar(25) DEFAULT NULL ,
  `author_name` varchar(25)  DEFAULT NULL ,
  primary key(`book_id`)
);

 
CREATE TABLE  `book_atributes`(

   `atributes_id` int NOT NULL AUTO_INCREMENT ,
  `published_year`YEAR DEFAULT NULL,
  `book_price` float(7) DEFAULT NULL,
  primary key(`atributes_id`)
);


CREATE TABLE  `sales`(
    `client_post_index` tinyint NOT NULL  ,
   `sale_count` tinyint DEFAULT NULL  UNIQUE,
   `sale_date` DATE DEFAULT NULL  UNIQUE,
   `client_post_index_address` varchar(35) DEFAULT NULL  UNIQUE,
    primary key(`client_post_index`)
);

CREATE TABLE  `client`(
`client_id` int NOT NULL AUTO_INCREMENT ,
  `client_name` varchar(20) DEFAULT NULL ,
  `client_phone` tinyint(15) DEFAULT NULL UNIQUE ,
  `client_post_index` tinyint DEFAULT NULL ,
  `client_post_index_phone` tinyint(15) DEFAULT NULL UNIQUE,
   primary key(`client_id`),
   FOREIGN KEY(`client_post_index`) REFERENCES  `sales`(`client_post_index`)
);

CREATE TABLE `connection`(
 `book_id` int NOT NULL ,
 `client_id` int NOT NULL , 
 `atributes_id` int NOT NULL ,
 
   PRIMARY KEY( `book_id` , `client_id` , `atributes_id` ),
   FOREIGN KEY(`book_id`) REFERENCES `book`(`book_id`),
   FOREIGN KEY(`client_id`) REFERENCES `client`(`client_id`),
   FOREIGN KEY( `atributes_id`) REFERENCES  `book_atributes`(`atributes_id`)

);



